
let input = [];
input.push("ab1231da");
input.push("abAB777da7");
let T = input.length;

for(i=0;i<T;i++){
    console.log(sum(input[i]));
}

function sum(s){
    let sum = 0;
    for(let i=0;i<s.length;i++){
        if(!isNaN(parseInt(s[i],10))){
            sum+=parseInt(s[i]);
        }
    }

    return sum;
  
}