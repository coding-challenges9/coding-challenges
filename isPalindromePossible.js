
console.log(isPalindromePossible("rearcac"));
console.log(isPalindromePossible("suhbeusheff"));
console.log(isPalindromePossible("palindrome"))


function isPalindromePossible(word)
{
  let count = Array(26).fill(0);
  let alphabet = Array.from(Array(26)).map((_, i) => String.fromCharCode(i + 97));
  
  for(let i=0;i<word.length;i++){
    for(let j=0;j<26;j++){
      if(word[i]===alphabet[j])
      {
        count[j]++;
      }
    }
  }

  let odd=0;
  for(let i=0;i<26;i++)
  {
    if((count[i]&1)===1){odd++}
  }

  if(odd>1){return "false";}

  return "true"

}
