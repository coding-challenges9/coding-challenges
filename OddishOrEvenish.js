oddishOrEvenish(43);
oddishOrEvenish(373);
oddishOrEvenish(4433);

function oddishOrEvenish(number){
    let a = Array.from(String(number), Number);
    let sum = 0;
    for(let i=0;i<a.length;i++){
        sum += a[i];
    }
    return sum%2?console.log('oddish'):console.log('evenish');

}
